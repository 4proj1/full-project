# Specifications

## Website

The website is divided into two interfaces:
- The administration part
- The customer part

In the administration part, you can add or delete product, manage user and adjust the stock.  

In the customers part, they can purchase product like a normal supermarket. They can see all their past orders too.
Users have to create an account on it before to access all functionnalities.

## Mobile Application

If you are a connected to an user account, you can find yourself on a mini-map of the store.  
You also have a list of available product to find it easily in the store.  
You can access to all functionalities of the website in the mobile app.

## Shop Simulator

You can simulate a customer experience, follow path, take or put products and look at what you taken.  
It help you to visualize and put yourself in front of your idea of what your supermarket v4.0 will look like !

## Artificial intelligence

The artificial intelligence learn from all the previous orders so it can recommand a product.  
It track customers inside the store to permanently know where they are.  
It track who is taking which product to properly build the order.  
It make the order and validate the payment.
It is moodulable.

## Back up

A back-up application automatically copy all databases files to an AWS S3 bucket everyday at 2 AM to prevent data loses.

## Beacons

We simulate the beacons in the shop simulator but in reality it cost 10 000 $.