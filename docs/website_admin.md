# Website Adminitration part

By default, the url of the Adminitration part is [http://localhost:8002/admin](http://localhost:8002/admin)

After login, the admin panel is a secure zone, you can see on the left, a sidebar with all manageable entities.

![image](img/admin_part_entities.png)

When you click on one of them (ex: "Products"), you can see a listing like this :

![image](img/listing.png)

for each row you can edit or delete it.

On the top of the table you can create a new object (here a new "Product") 

When you went create new object, you will have a form like this :

![image](img/new_object.png)

