# Shop simulator

By default, the url of the shop simulator is [http://localhost:8003](http://localhost:8003)

The shop configurator url is http://localhost:8001/tile/ and the ia url is http://localhost:8000
The user id is the id in the web database of the user that will be simulated in the shop. 
Fill those the fields like this : ![image](img/simulator1.png)

Then click on the "Run simulation" button.
The shop must appear as it have been configured in the shop configurator.
You can walk using arrows, pick an item in a shelf with "e", put an item on a shelf with "a" and show all items you have with "i".
When you exit the shop, items that you have are purchased.