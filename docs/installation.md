# Installation

## Requirements

Docker desktop or docker in a virtual machine with port forwarding on a windows host is required.
You also need an android emulator. Visual studio is recommended to ease installation of the mobile app.

## Containers

### 1) Fill docker-compose file

In the file named "docker-compose.yml", you have to fill 3 environment variables : 
- ACCESS_KEY : AWS access key to access a S3 bucket
- SECRET_KEY : AWS secret key to access a S3 bucket
- S3_NAME : Name of the S3 bucket

### 2) docker-compose command

Type the command "docker-compose up -d" in the same folder than the docker-compose file. All images are then pulled and containers are run.

## Mobile app

The easiest way to install the mobile app is to open the source code with visual studio. But you can also install the apk on your own emulator.
