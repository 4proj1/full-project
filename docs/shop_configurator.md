# Shop configurator

By default, the url of the shop configurator is [http://localhost:8001](http://localhost:8001)

The table is a representation of a shop. It determines the global shape of the shop. You can add columns and rows to make the shop bigger.  
By default, all the tiles are walls (in black). You can change it into shelves or path by clicking on it.  
Shelves can also have products on it, click on shelves to add a product on it.    
You can choose to let the default product that is represented with cubes.  
Either way there is few products that we made for you.  
You can put some of the following aviaible products (with this exact sentence) : sugar, lait, can, pois, cereal, ravioli, pommme.
Those products names must exist **WITH THE SAME NAME** in the web database.


When the shop have a good shape, click on the "Upload to database" button to persist it. 

