# Website Customers part

By default, the url of the shop simulator is [http://localhost:8002](http://localhost:8002)

By default you have all buyable product. Each row is one product. On clicked on you access of describ of the selected product

![image](img/customer_listing.png)

You can increment or decrement the number of product you want buy

![image](img/product.png)

On the bottom side you have the similar products  

![image](img/simili.png)

On the Top of the screen, after login, you can check your passed orders, your current cart, and your profile 

![image](img/nav.png)
