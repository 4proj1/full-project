# Mobile app

At the start of the mobile app, before to log on, you have to change the settings of the app by clicking the "settings' button.  
In the settings page, there is 2 fields that needs to be updated. You have to replace 172.20.10.4 by the ip address of the host which run containers.  
After it, you can click on the "save" button and log in with some user credentials. This user must have been created on the website before.  

You can find 4 sections on the burger menu : 
![image](img/mobile_menu.PNG)

## Web Browser

It shows the web site like in a web browser.

## Browse

It show you a list of available products.
![image](img/mobile_browse.PNG)

When you click on a product, a red cross show you where is this product in the shop and show where you are.
![image](img/mobile_tracking.PNG)

## Shop

This is a view of the shop like the last picture right above.

## About

In this section, you can see information about this app, like the technology used and where the icons come from.